//
//  Main_Menu.swift
//  UnLicensedParker
//
//  Created by Christian on 28/01/2020.
//  Copyright © 2020 Silvio Fosso. All rights reserved.
//

import Foundation
import SpriteKit
import GameplayKit
import UIKit
import GameKit
import AVFoundation
class Main_Menu: SKScene,GKGameCenterControllerDelegate  {
    func gameCenterViewControllerDidFinish(_ gameCenterViewController: GKGameCenterViewController) {
        gameCenterViewController.dismiss(animated: true, completion: nil)
    }
    
    
    
        var speakerButton: MSButtonNode!
    let c = UIViewController()
    var Suono = AVAudioPlayer()
    var sound = UserDefaults.standard.bool(forKey: "silenzioso")
   
    
    override func sceneDidLoad()
    {
        
        var buttonStart: MSButtonNode!
        var buttonLeader: MSButtonNode!
        var buttonSuono : MSButtonNode!
        NotificationCenter.default.addObserver(self, selector: #selector(showAuthenticationViewController),
                                               name: NSNotification.Name(GameKitHelper.PresentAuthenticationViewController),object: nil)
        
        GameKitHelper.sharedInstance.authenticateLocalPlayer()
        
        buttonStart = self.childNode(withName: "Start") as? MSButtonNode
    
        let titolo = self.childNode(withName: "un")
        titolo?.physicsBody?.affectedByGravity = true
        titolo?.physicsBody?.isDynamic = true
        titolo?.physicsBody = SKPhysicsBody(rectangleOf: CGSize(width:
            (titolo?.frame.width)!, height: (titolo?.frame.height)!))
        titolo?.physicsBody?.allowsRotation = false
        self.physicsWorld.gravity = CGVector(dx: 0, dy: -2)
        let bodo = SKShapeNode(rect: CGRect(x: 100, y: 0, width: 1000, height: 1))
        bodo.physicsBody = SKPhysicsBody(rectangleOf: CGSize(width: bodo.frame.width, height: bodo.frame.height))
        bodo.position = CGPoint(x: 50, y: 20)
        bodo.physicsBody?.affectedByGravity = false
        bodo.physicsBody?.isDynamic = false
       buttonSuono = self.childNode(withName: "Speaker") as? MSButtonNode
        let dec = Recognize.SharedInstance.main(Device: UIDevice.current.modelName)
        let screenSize = UIScreen.main.bounds
        let screenWidth = screenSize.width * 0.00002
        let screenHeight = screenSize.height * 0.35
       
        buttonSuono.position = CGPoint(x: screenWidth - dec  , y: screenHeight )
       
       
        addChild(bodo)
        buttonStart.selectedHandler = {
            guard let skView = self.view as SKView? else {
                print("Could not get Skview")
                return
            }
            
            
            
            var scene = GameScene(size: skView.bounds.size)
  //          self.suono()
            
            skView.presentScene(scene)
            
        }
       if (UserDefaults.standard.bool(forKey: "silenzioso")  == false)
       {
       // buttonSuono = self.childNode(withName: "Speaker") as? MSButtonNode
        self.sound = true
        }
        else
       {
        self.sound = false
        buttonSuono.texture = SKTexture(imageNamed: "speaker_off")
        }
        buttonSuono.selectedHandler = {
            if(self.sound == false)
            {
                self.sound = true
                buttonSuono.texture = SKTexture(imageNamed: "Speaker")
                UserDefaults.standard.set(false, forKey: "silenzioso")
               print("gatto")
            }
            else
            {
               self.sound = false
               buttonSuono.texture = SKTexture(imageNamed: "Speaker_off")
               UserDefaults.standard.set(true, forKey: "silenzioso")
               print("cane")
            }
             
        }
        
        
        buttonLeader = self.childNode(withName: "LeaderBoard") as? MSButtonNode
        
        
        buttonLeader.selectedHandler = {
            
            let viewController = GKGameCenterViewController()
            viewController.viewState = .leaderboards
            viewController.gameCenterDelegate = self
            viewController.leaderboardIdentifier="This"
            UIApplication.topViewController()?.present(
                viewController,
                animated: true, completion: nil)
          //  self.suono()
        }
       
        
        //        speakerButton = self.childNode(withName: "speaker") as? MSButtonNode
    }
    @objc func showAuthenticationViewController() {
        let gameKitHelper = GameKitHelper.sharedInstance
        
        if let authenticationViewController =
            gameKitHelper.authenticationViewController {
            UIApplication.topViewController()?.present(
                
                authenticationViewController,
                animated: true, completion: nil)
            
        }
        
    }
//    func suono ()
  /*  {
       if (UserDefaults.standard.bool(forKey: "silenzioso") == true )
       {
        let path = Bundle.main.path(forResource: "tap.wav", ofType:nil)!
        let url = URL(fileURLWithPath: path)
        do {
            
            Suono = try AVAudioPlayer(contentsOf: url)
            DispatchQueue.global().async {
                self.Suono.play()
            }
        }
        catch {
            print("nada")
            
        }
        }
    }*/
    
    override func didMove(to view: SKView) {
        
        
    }
}
extension UIApplication {
    class func topViewController(base: UIViewController? = UIApplication.shared.keyWindow?.rootViewController) -> UIViewController? {
        if let nav = base as? UINavigationController {
            return topViewController(base: nav.visibleViewController)
        }
        if let tab = base as? UITabBarController {
            if let selected = tab.selectedViewController {
                return topViewController(base: selected)
            }
        }
        if let presented = base?.presentedViewController {
            return topViewController(base: presented)
        }
        return base
    }
}



