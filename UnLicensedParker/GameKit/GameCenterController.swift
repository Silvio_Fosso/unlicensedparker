//
//  GameCenterController.swift
//  UnLicensedParker
//
//  Created by Luigi Mazzarella on 11/02/2020.
//  Copyright © 2020 Silvio Fosso. All rights reserved.
//

import UIKit
import GameKit
class GameCenterController: UINavigationController {
    
    

    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector: #selector(showAuthenticationViewController),
            name: NSNotification.Name(GameKitHelper.PresentAuthenticationViewController),object: nil)

          GameKitHelper.sharedInstance.authenticateLocalPlayer()
    
        

      
    }
   
    @objc func showAuthenticationViewController() {
      let gameKitHelper = GameKitHelper.sharedInstance

      if let authenticationViewController =
          gameKitHelper.authenticationViewController {
        topViewController?.present(
          authenticationViewController,
          animated: true, completion: nil)
      }
        
    }
    
    deinit {
         NotificationCenter.default.removeObserver(self)
       }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
}
