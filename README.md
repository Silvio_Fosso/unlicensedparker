# UnlicensedParker

This app is written in swift on xcode

It’s a beautiful day, passed without problems and the sunset is going down, why not go for an aperitif downtown? Take the car and easily find parking. 

But there is a hidden evil that is approaching. 

The valet squatter! Beat the squatters you meet along the way and save coins for your aperitif! 

Challenge friends: Exceed the score of your friends and who loses pays pledge: pay the aperitif for the whole group!

![](video/1.MP4)
